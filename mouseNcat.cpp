///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02d - Mouse `n Cat - EE 205 - Spr 2022
///
/// This C++ program will either:
///   1.  Print the command line arguments in reverse order
///   2.  or, if there are no command line arguments, print all of the
///       environment variables passed into the program
///
/// @file    mouseNcat.cpp
/// @version 1.0 - Initial version
///
/// Compile: $ g++ -o mouseNcat mouseNcat.cpp
///
/// Usage:  mouseNcat [param1] [param2] ...
///
/// Example:
///   With a command line parameter:
///   $ ./mouseNcat I am Sam
///   Sam
///   am
///   I
///   $
///
///   Without a command line parameter:
///   $ ./mouseNcat
///   SHELL=/bin/bash
///   ...
///   SSH_TTY=/dev/pts/0
///
/// @author  Reid Lum <reidlum@hawaii.edu>
/// @date    21 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <iostream>
#include <cstdlib>

// Note the new main() parameter:  envp
int main( int argc, char* argv[], char* envp[] ) {
    if (argc > 1) {
        for(int i = 1; i <= (argc - 1); i++){
            std::cout << argv[argc - i] << std::endl;
        }
    }
    
    else {
        for(int j = 0; envp[j + 3] != NULL; j++){
            std::cout << envp[j] << std::endl;
        }
        /*
        The j + 3 in the for loop condition is to get the last line
        to be SSH_TTY as is it in the example, but in the pdf instruction
        it sounds like you wanted all of envp. To do this I'd delete + 3 
        */
    }
    

   std::exit( EXIT_SUCCESS );
}



